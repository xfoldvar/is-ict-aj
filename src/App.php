<?php
use src\FileProcessor;
use Filter\Filter;

/**
 * Created by PhpStorm.
 * User: xfoldvar
 * Date: 5/1/2018
 * Time: 8:44 PM
 */

namespace src;
include_once "FileProcessor/Processor.php";
include_once "Bayes/Trainer.php";
use src\Bayes\Trainer;
use src\FileProcessor\EmailMessage;
use src\FileProcessor\Processor;

//include_once "Filter/Filter.php";
//use src\Filter\Filter;
class App
{

    public static function run($path){
//        $processor = new Processor($path);
//        $filter = new Filter();
//
//		//self::debug($processor->getSpam(),'spam');
//
//		$learnedFilter = $filter->learnSpamFilter($processor->calculateWordStats($processor->getSpam()),300);
//
//		//self::debug($learnedFilter);
//
//		$learnedFilterN = $filter->learnSpamFilter($processor->calculateWordStats($processor->getNotSpam()),250);
//
//		//self::debug($learnedFilterN);
//
//		$fil = array_diff($learnedFilter, $learnedFilterN);
//
//		//self::debug($fil);
//
//		$spamFilter = $filter->buildSpamFilter();
//		$spamFilter['learned'] = array_slice($fil,0,10);
//
//		$spamFilter = $filter->removeCommonWordsFromFilter($spamFilter);
//
//		self::debug($spamFilter, "Spam filters");
//
//
//
//		//self::debug($filter->filterDataSet($processor->getFileAsArray(), $spamFilter), "SPAM FILTER RESULT");
//
//		$result = $processor->diffResult($filter->filterDataSet($processor->getFileAsArray(), $spamFilter));
//
//		self::debug($result['spam'], "Correctly identified spam");
//		self::debug($result['notSpam'], "InCorrectly identified spam");
//
//
//        //self::debug($processor->getNotSpam(),'Not spam');
//
//        //self::debug($processor->getFileWithoutClassification(),'DATASET');
//
//		echo "<h4>"; echo "Real spam mail count: " . count($processor->getSpam()) . "</h4>";

        self::runBayes($path);
    }

    public static function runBayes($path){
        $processor = new Processor($path);

        $collection = $processor->getObjectCollection();

        //split data set to 70% of training data and 30% of testing data
        $trainData = array_slice($collection, 0, 3000); //circa 69%
        $testData = array_slice($collection, 3001); //31%

        $trainer = new Trainer($trainData); //construct trainer and train on training data

        /**
         * @var $message EmailMessage
         */
        $result = array();
        $spamCountReal = 0;
        $spamCountClassified = 0;
        $realSpam = array();
        $realHam = array();

        $classifiedSpam = array();
        $classifiedHam = array();

        $successSpamCount = 0;
        $successHamCount = 0;

        $falsePositive = 0;
        foreach ($testData as $message){
            if($message->isSpam()){
                $spamCountReal++;
                $realSpam[] = $message;
            } else {
                $realHam[] = $message;
            }
            $isSpam = $trainer->classify($message->getBody(),1);
            $resultMessage = new EmailMessage($message->getBody(), $isSpam);
            if($isSpam){
                $spamCountClassified++;
                $classifiedSpam[] = $resultMessage;
            } else {
                $classifiedHam[] = $resultMessage;
            }

            ////stats////

            if($message->isSpam() && $resultMessage->isSpam()){
                $successSpamCount++;
            } else if(!$message->isSpam() && !$resultMessage->isSpam()){
                $successHamCount++;
            }

            if(!$message->isSpam() && $resultMessage->isSpam()){
                $falsePositive++;
            }

            $result[] = $resultMessage;
        }
        self::debug($testData, "Test Input Data");
        self::debug($result, "Classified Result");

        self::debug($classifiedSpam, "Classified SPAM");
        self::debug($realSpam, "Real SPAM");

        self::debug($spamCountClassified, "Spam Count of Classified");
        self::debug($spamCountReal, "Spam Count of Real");

        self::debug((($successSpamCount + $successHamCount)/count($testData))*100, "Classification ACCURACY", "%");
        self::debug(($falsePositive/count($realHam))*100, "Classification FALSE POSITIVE", "%");
    }


    private static function debug($data, $indice = "", $units = ""){
        if(is_array($data)){
            echo "<div style='overflow: auto; height:500px; border-style: inset;float:left; width:600px;'>";
        } else {
            echo "<div style='overflow: auto; height:100px; border-style: inset;float:left; width:600px;'>";
        }
        if($indice != ""){
            echo "<h3>";
            echo $indice;
            echo "</h3>";
        }
        if(is_array($data)){
            echo "<h4>"; echo "Collection count: " . count($data) . "</h4>";
            echo "<pre>";
            foreach ($data as $d){
                echo $d . "\n";
            }
            echo "</pre>";
        } else {
            echo "<p>" . $data . $units . "</p>";
        }
        echo "</div>";
    }

}