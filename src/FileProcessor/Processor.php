<?php
/**
 * Created by PhpStorm.
 * User: xfoldvar
 * Date: 5/1/2018
 * Time: 6:49 PM
 */

namespace src\FileProcessor;
include_once "File.php";
include_once "EmailMessage.php";

class Processor
{
    /**
     * @var File
     */
    private $file;

    /**
     * @var array
     */
    private $spam = null;

    /**
     * @var array
     */
    private $notSpam = null;

    /**
     * @var array
     */
    private $objectCollection = null;

    public function __construct($path)
    {
        try{
            $this->file = new File($path);
        } catch (LoadFileException $e){
            echo $e->getMessage() . "\n";
        }
    }

    /**
     * @return array
     */
    public function getSpam(){
        if($this->spam == null){
            $this->classify();
        }
        return $this->spam;
    }

    /**
     * @return array
     */
    public function getNotSpam(){
        if($this->notSpam == null){
            $this->classify();
        }
        return $this->notSpam;
    }

    /**
     * Classify dataset to spam and not spam properties
     */
    private function classify(){
        $fileAsArray = $this->file->toArray();
        foreach ($fileAsArray as $line){
			$text = $this->removeHtmlTags($line);
            $class = mb_substr($text,0,1);
            $isSpam = false;
            if($class == "0" || $class == " 0"){
                $isSpam = true;
                $this->spam[] = $text;
            } else {
                $this->notSpam[] = $text;
            }
            $this->objectCollection[] = new EmailMessage($text, $isSpam);
        }
    }

    /**
     * Return dataset without classification
     * @return array
     */
    public function getFileWithoutClassification(){
        if(empty($this->spam)){
            $this->classify();
        }
        $result = array();
        foreach ($this->spam as $spamLine){
            $result[] = ltrim($spamLine,"0");
        }
        foreach ($this->notSpam as $notSpamLine){
            $result[] = ltrim($notSpamLine,"1");
        }
        return $result;
    }
	
	public function getFileAsArray(){
		return  array_merge($this->spam, $this->notSpam);
	}

    /**
     * @param $source array
     * @return array
     */
    public function calculateWordStats($source){
        $result = array();

        foreach ($source as $line) {
            $line = $this->removeHtmlTags($line);
            $line = strtolower($line);
            $words = explode(" ", $line);

            foreach ($words as $word){
                $word = $this->removeUnnecessaryCharFromWord($word);
				if(mb_strlen($word)>=4 && !is_numeric($word)){					
					if(isset($result[$word])){
						$result[$word]++;
					} else {
						$result[$word] = 1;
					}
				}
            }
        }

        return $result;
    }

    private function removeHtmlTags($text){
		/*$text = $this->xss_clean($text);
		$txt = htmlentities($text, null, 'utf-8');   
        $txt = htmlspecialchars_decode($txt);
		
		$noSpecial = preg_replace('/(?:<|&lt;).*?(?:>|&gt;)/', '', $txt);;
		$noHtml = strip_tags(html_entity_decode($noSpecial));
		$noJs = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $noHtml);
		$result = preg_replace('/[^A-Za-z\-]/', '', $noJs);
		*/
        return strip_tags(mb_strtolower($text));
    }

    /**
     * @return array
     */
    public function getObjectCollection()
    {
        if(is_null($this->objectCollection)){
            $this->classify();
        }
        return $this->objectCollection;
    }



    private function removeUnnecessaryCharFromWord($word){
        $word = $this->removeHtmlTags($word);
        $word = str_replace('.','',$word);
		$word = str_replace(':','',$word);
        $word = str_replace(' ','',$word);
        $word = str_replace('!','',$word);
        $word = str_replace('?','',$word);
        $word = str_replace('','',$word);
		$word = str_replace('&nbsp;','',$word);
		$word = str_replace('\t','',$word);
		$word = str_replace('\n','',$word);
		$word = str_replace('	','',$word);
		$word = preg_replace('/[^A-Za-z0-9\-]/', '', $word);
		$word = preg_replace('/-+/', '', $word); 
        return $word;
    }
	
	
	public function diffResult($source){
		$result = array();
		foreach ($source as $line){
            $class = mb_substr($line,0,1);
            if($class == "0" || $class == " 0"){
                $result['spam'][] = $line;
            } else {
                $result['notSpam'][] = $line;
            }
        }
		return $result;
	}
}