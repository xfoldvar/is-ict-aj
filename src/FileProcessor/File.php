<?php
/**
 * Created by PhpStorm.
 * User: xfoldvar
 * Date: 5/1/2018
 * Time: 6:49 PM
 */

namespace src\FileProcessor;
include_once "Exception.php";


class File
{
    /**
     * @var resource
     */
    private $resource;

    private $path;

    public function __construct($path)
    {
        $this->path = $path;
        try{
            $this->loadFile($path);
        }catch (LoadFileException $e){
            echo "Error opening file. Exception: " . $e->getMessage();
        }
    }

    /**
     * Try to load file as resource
     * @param $path
     * @throws LoadFileException
     */
    private function loadFile($path){
        try{
            $this->resource = fopen($path, "r");
            if(!$this->resource){
                throw new LoadFileException("Cannot open file at specified path:" . $path);
            }
			fclose($this->resource);
        } catch (\Exception $e){
            throw new LoadFileException($e->getMessage());
        }
    }

    /**
     * @return string
     * @throws EndOfFile
     * @throws ResourceNotLoaded
     */
    private function readLine(){
        if(!$this->resource){
            throw new ResourceNotLoaded("Resource not loaded!");
        }
		$line = "";
        if(!feof($this->resource)){
            $line = fgets($this->resource);
        } else {
            throw new EndOfFile("End of file!");
        }
		return $line;
    }

    /**
     * File Lines to array
     * @return array
     * @throws ResourceNotLoaded
     */
    public function toArray(){
        $eof = false;
        $result = array();
		$this->resource = fopen($this->path, "r");
        while (!$eof){
            try{
                $line= $this->readLine();
                $result[] = $line;
            } catch (EndOfFile $e){
                $eof = true;
            } catch (ResourceNotLoaded $r){
				fclose($this->resource);
				throw $r;
            }
        }
		fclose($this->resource);
        return $result;		
    }
}