<?php
/**
 * Created by PhpStorm.
 * User: xfoldvar
 * Date: 5/1/2018
 * Time: 6:53 PM
 */

namespace src\FileProcessor;


class LoadFileException extends \Exception {}

class ResourceNotLoaded extends \Exception {}

class EndOfFile extends  \Exception {}