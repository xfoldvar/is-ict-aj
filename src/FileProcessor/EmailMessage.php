<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 29.5.18
 * Time: 22:43
 */

namespace src\FileProcessor;

include_once __DIR__ . "/../Filter/NotSpam/Base.php";
use src\Filter\NotSpam\Base;

class EmailMessage
{

    private $body;

    private $isSpam;

    public function __construct($message, $isSpam = FALSE)
    {
        $this->body = $message;
        $this->isSpam = $isSpam;
    }

    /**
     * @param bool $isSpam
     */
    public function setIsSpam($isSpam)
    {
        $this->isSpam = $isSpam;
    }



    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return bool
     */
    public function isSpam()
    {
        return $this->isSpam;
    }

    public function getWords(){
        $source = $this->preProcessMessage();
        $result = $this->tokenize($source);
        return $result;
    }

    private function removeUnnecessaryCharFromWord($word){
        $word =  strip_tags(mb_strtolower($word));
        $word = str_replace('.','',$word);
        $word = str_replace(':','',$word);
        $word = str_replace(' ','',$word);
        $word = str_replace('!','',$word);
        $word = str_replace('?','',$word);
        $word = str_replace('','',$word);
        $word = str_replace('&nbsp;','',$word);
        $word = str_replace('\t','',$word);
        $word = str_replace('\n','',$word);
        $word = str_replace('	','',$word);
        $word = preg_replace('/[^A-Za-z0-9\-]/', '', $word);
        $word = preg_replace('/-+/', '', $word);
        return $word;
    }

    private function tokenize($body){
        $token = strtok($body, " ");
        $stopWords = Base::getData();
        $result = array();
        while ($token !== false){
            if(mb_strlen($token)>=2) {
                if(!in_array($token, $stopWords)){
                    $result[] = $token;
                }
            }
            $token = strtok(" ");
        }
        return $result;
    }

    const STR_SEPARATOR = "---";

    public function __toString()
    {
        return "Class - " . ($this->isSpam ? "SPAM!!" : "HAM!!") . ' ' . self::STR_SEPARATOR . ' ' . $this->getBody();
    }

    private function preProcessMessage(){
        $temp = preg_replace("/[^a-zA-Z 0-9]+/","",$this->getBody());
        return strip_tags(mb_strtolower($temp));
    }


}