<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 29.5.18
 * Time: 22:36
 */

namespace src\Bayes;


use src\FileProcessor\EmailMessage;

class Trainer
{

    private $totalCountOfMessages;

    private $spamMessagesCount;

    private $hamMessagesCount;

    private $probabilityOfSpam;

    private $probabilityOfNotSpam;

    private $listSpamWords;

    private $listHamWords;

    private $spamWordCount = 0;

    private $hamWordCount = 0;

    private $totalWordCount = 0;

    /**
     * @var EmailMessage[]
     */
    private $dataSet;

    private $uniqueWordsCount = 0;

    /**
     * Trainer constructor.
     * @param $trainingDataSet EmailMessage[]
     */
    public function __construct($trainingDataSet)
    {
        $this->dataSet = $trainingDataSet;
        $this->listSpamWords = array();
        $this->listHamWords = array();
        $this->totalWordCount = 0;
        $this->spamWordCount = 0;
        $this->hamWordCount = 0;
        $this->train();
    }

    /**
     * In this method we are processing training dataSet
     * In a loop we are iterating over EmailMessage objects and calculating occurence of spam/ham messages count,
     * total count of all processed messages and calling method for processing words of email message
     * In the end global probabilities are computed => probabilityOfSpam and probabilityOfNotSpam
     */
    private function train(){
        $this->totalCountOfMessages = 0;
        $this->spamMessagesCount = 0;

        /**
         * @var $message EmailMessage
         */
        foreach ($this->dataSet as $message){
            if ($message->isSpam()){
                $this->spamMessagesCount++;
            } else {
                $this->hamMessagesCount++;
            }
            $this->totalCountOfMessages++;
            $this->trainWordsOfMessage($message);
        }

        $this->probabilityOfSpam = $this->spamMessagesCount / $this->totalCountOfMessages;

        $this->probabilityOfNotSpam = ($this->totalCountOfMessages - $this->spamMessagesCount) / $this->totalCountOfMessages;
    }

    /**
     * This method is processing training data words
     * The input parameter is EmailMessage object
     * EmailMessage->getWords() method provides tokenization, filtering by stop words, etc
     * In this simple method we calculate word occurrence of each test email message
     * Based on information if email is spam or ham, words are stored in relevant associative arrays
     * Also, total words count and unique words count are computed here
     * @param EmailMessage $message
     */
    private function trainWordsOfMessage(EmailMessage $message){
        foreach ($message->getWords() as $word){
            if($message->isSpam()){
                if(isset($this->listSpamWords[$word])){
                    $this->listSpamWords[$word] = $this->listSpamWords[$word] + 1;
                } else {
                    $this->listSpamWords[$word] = 1;
                    $this->uniqueWordsCount++;
                }
                $this->spamWordCount++;
            } else {
                if(isset($this->listHamWords[$word])){
                    $this->listHamWords[$word] = $this->listHamWords[$word] + 1;
                } else {
                    $this->listHamWords[$word] = 1;
                    $this->uniqueWordsCount++;
                }
                $this->hamWordCount++;
            }
            $this->totalWordCount++;
        }
    }

    /**
     * Getter for easier work with SpamWordList property
     * @param $word
     * @return int
     */
    private function getWordFromSpamWordList($word){
        return isset($this->listSpamWords[$word]) ? $this->listSpamWords[$word] : 1;
    }

    /**
     * Getter for easier work with HamWordList
     * @param $word
     * @return int
     */
    private function getWordFromHamWordList($word){
        return isset($this->listHamWords[$word]) ? $this->listHamWords[$word] : 1;
    }

    /**
     * Method for calculation of probability that word belongs to spam words list or to ham word list
     * Here is the place where Laplace smoothing comes in - if $alpha parameter is not null
     * @param $word
     * @param $isSpam
     * @param null $alpha If is different from NULL -> it will enable laplace smoothing
     * @return float|int
     */
    private function decideOnWord($word, $isSpam, $alpha = NULL){
        if(is_null($alpha)){
            if($isSpam){
                return ($this->getWordFromSpamWordList($word)) / $this->spamWordCount;
            }
            return ($this->getWordFromHamWordList($word)) / $this->hamWordCount;
        } else {
            if($isSpam){
                return ($this->getWordFromSpamWordList($word) + $alpha)  /
                    ($this->spamWordCount + ($this->uniqueWordsCount * $alpha));
            }
            return ($this->getWordFromHamWordList($word) + $alpha) /
                ($this->hamWordCount + ($this->uniqueWordsCount * $alpha));
        }
    }

    /**
     * Decision is made in log space for better accuracy
     * 1. First of all we create an EmailMessage object from message text provided in first parameter of this function
     * 2. Then we calculate global spam and ham probability occurrence in log space
     * 3. After that we call get words from EmailMessage object which do the tokenization (split sentences in to words,
     *    filter by stop words, take words which length is 3 or more chars, ...)
     * 4. In a loop we calculate probability of spam/ham word based on learned data which are stored in associative
     *    array with value of occurrence AND add this probability to global spam/ham prob in log space
     * 5. Final step is to compare global computed probabilities => TRUE if message is SPAM | FALSE if message is HAM
     * @param $message
     * @param null $alpha
     * @return bool
     */
    private function decideOnEmailMessage($message, $alpha = NULL){
        $message = new EmailMessage($message);

        $pSpam = log($this->probabilityOfSpam);
        $pHam = log(1 - $this->probabilityOfSpam);

        foreach ($message->getWords() as $word) {
            $pSpam += log($this->decideOnWord($word, true, $alpha));
        }
        foreach ($message->getWords() as $word) {
            $pHam += log($this->decideOnWord($word, false, $alpha));
        }
        return $pSpam > $pHam;
    }

    /**
     * Public method for classifying new email messages
     * @param string $messageText
     * @param float $alpha
     * @return bool true if isSpam false if is ham
     */
    public function classify($messageText, $alpha = NULL){
        return $this->decideOnEmailMessage($messageText, $alpha);
    }

}