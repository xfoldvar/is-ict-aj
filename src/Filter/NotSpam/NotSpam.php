<?php
/**
 * Created by PhpStorm.
 * User: xfoldvar
 * Date: 5/1/2018
 * Time: 8:18 PM
 */

namespace src\Filter\NotSpam;


class NotSpam
{

    /**
     * @var array
     */
    private static $data;

    /**
     * @return array
     */
    public static function getData()
    {
        return self::$data;
    }

}