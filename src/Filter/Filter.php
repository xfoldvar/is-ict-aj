<?php

/**
 * Created by PhpStorm.
 * User: xfoldvar
 * Date: 5/1/2018
 * Time: 7:32 PM
 */

namespace src\Filter;

include_once "Spam/Sex.php";
include_once "Spam/Money.php";
include_once "Spam/Base.php";
include_once "NotSpam/Base.php";


use src\Filter\NotSpam\Base;
use src\Filter\Spam\Money;
use src\Filter\Spam\Sex;
use src\Filter\Spam\Base as SpamBase;

class Filter
{


    public function buildSpamFilter(){
        $result = array();
        $result['sex'] = Sex::getData();
        $result['money'] = Money::getData();
        return $result;
    }

    public function buildNotSpamFilter(){
        $result =array();
        $result['base'] = Base::getData();
        return $result;
    }

    public function filterDataSet($dataSet, $filter){
        $result = array();
		$indexer = 0;
        foreach ($dataSet as $data){
			$indexer++;
			$re = mb_strpos($data, 're:');
			if(!$re)
				foreach ($filter as $kind){
					foreach($kind as $rude){
						$r = mb_strpos($data, $rude);
						if($r){
							$result[$indexer] = substr($data,0,25) . " ... ";
						}	
					}
				}
        }
        return $result;
    }

    /**
     * Method for building spam filter
     * @param $spamWordStats
     * @param int $filterSize
     * @return array
     */
    public function learnSpamFilter($spamWordStats, $filterSize = 20){
        foreach ($this->buildNotSpamFilter()['base'] as $notSpam){
            if(isset($spamWordStats[$notSpam])){
                unset($spamWordStats[$notSpam]);
            }
        }
        //we have to sort stats
        arsort($spamWordStats);

        $highRated = array_slice($spamWordStats,0,$filterSize);

        $filter = array_keys($highRated);

        return $filter;
    }

    /**
     * Method for filtering filter data with stop words
     * @param $filters
     * @return mixed
     */
	public function removeCommonWordsFromFilter(&$filters){
		foreach ($this->buildNotSpamFilter()['base'] as $notSpam){
            foreach($filters as &$filter){				
				if(in_array($notSpam, $filter)){
					$key = array_search($notSpam, $filter);
					unset($filter[$key]);
				}
			}
        }
		return $filters;
	}


}