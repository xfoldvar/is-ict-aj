<?php
/**
 * Created by PhpStorm.
 * User: xfoldvar
 * Date: 5/1/2018
 * Time: 7:40 PM
 */

namespace src\Filter\Spam;
include_once "Spam.php";

class Base extends Spam
{
    private static $data = array( '#1','$$$','100%','act now','action','additional income','affordable','natural','amazed','apply now','avoid','amazed','beneficiary','billing','billion','bonus','boss','buy','call-now','cancel','cash','casino','certified','cheap','click','clearance','collect','congratulations','offers','cures','deal','dear friend','debt','discount','delete','hesitate','income','earn','extra','expire','fantastic','access','gift','freedom','friend','great','guarantee','hello','income','increase','instant','investment','junk','limited','lose','lowest price','luxury','$','€','medicine','money','name','experience','now','obligation','offer','only','open','order-now','please','presently','problem','promise','purchase','quote','rates','refinance','refund','remove','request','risk-free','sales','satisfaction','save','score','serious','spam','success','supplies','action','terms','traffic','trial','unlimited','urgent','weight','supplies','win','winner' );
	
	public static function getdata()
    {
        return self::$data;
    }
}