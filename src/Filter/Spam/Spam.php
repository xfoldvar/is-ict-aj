<?php
/**
 * Created by PhpStorm.
 * User: xfoldvar
 * Date: 5/1/2018
 * Time: 7:35 PM
 */

namespace src\Filter\Spam;


class Spam
{
    /**
     * @var array
     */
    private static $data;

    /**
     * @return array
     */
    public static function getData()
    {
        return self::$data;
    }
}